<?php

namespace Drupal\course_management;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the student entity type.
 */
class StudentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view student');

      case 'update':
        $permissions = ['edit student', 'administer student'];
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'delete':
        $permissions = ['delete student', 'administer student'];
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $permissions = ['create student', 'administer student'];
    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
