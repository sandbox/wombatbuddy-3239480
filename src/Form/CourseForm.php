<?php

namespace Drupal\course_management\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the course entity edit forms.
 */
class CourseForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New course %label has been created.', $message_arguments));
      $this->logger('course_management')->notice('Created new course %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The course %label has been updated.', $message_arguments));
      $this->logger('course_management')->notice('Updated new course %label.', $logger_arguments);
    }

    // Redirect a user to the "students" view for display students of the
    // current course and also the "Add Students" and the "Add Course" buttons.
    // The current course will be displayed above the "student" view via the
    // block of the "course" view.
    $form_state->setRedirect('view.students.page_1', ['course' => $entity->id()]);
  }

}
