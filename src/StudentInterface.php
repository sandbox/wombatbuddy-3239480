<?php

namespace Drupal\course_management;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a student entity type.
 */
interface StudentInterface extends ContentEntityInterface {

}
