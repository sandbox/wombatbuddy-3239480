<?php

namespace Drupal\course_management;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a course entity type.
 */
interface CourseInterface extends ContentEntityInterface {

}
