<?php

namespace Drupal\course_management;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a task entity type.
 */
interface TaskInterface extends ContentEntityInterface {

  /**
   * Gets the task title.
   *
   * @return string
   *   Title of the task.
   */
  public function getTitle();

  /**
   * Sets the task title.
   *
   * @param string $title
   *   The task title.
   *
   * @return \Drupal\course_management\TaskInterface
   *   The called task entity.
   */
  public function setTitle($title);

}
