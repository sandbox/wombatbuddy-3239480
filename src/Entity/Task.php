<?php

namespace Drupal\course_management\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\course_management\TaskInterface;

/**
 * Defines the task entity class.
 *
 * @ContentEntityType(
 *   id = "task",
 *   label = @Translation("Task"),
 *   label_collection = @Translation("Tasks"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\course_management\TaskListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\course_management\TaskAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\course_management\Form\TaskForm",
 *       "edit" = "Drupal\course_management\Form\TaskForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "task",
 *   admin_permission = "administer task",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/task/add",
 *     "canonical" = "/task/{task}",
 *     "edit-form" = "/admin/content/task/{task}/edit",
 *     "delete-form" = "/admin/content/task/{task}/delete",
 *     "collection" = "/admin/content/task"
 *   },
 *   field_ui_base_route = "entity.task.settings"
 * )
 */
class Task extends ContentEntityBase implements TaskInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the task entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['course'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Course'))
      ->setDescription(t('The course.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'course')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(1);

    $fields['details'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Details'))
      ->setDescription(t('A details of the task.'))
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'text_textarea',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['progress'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Progress'))
      ->setDescription(t('The progress of the task.'))
      ->setRequired(TRUE)
      ->setSettings([
        'min' => 0,
        'max' => 100,
        'prefix' => '',
        'suffix' => '%',
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'number_integer',
        'prefix_suffix' => TRUE,
        'thousand_separator' => '',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'prefix_suffix' => TRUE,
        'thousand_separator' => '',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date'))
      ->setDescription(t('Date of the task.'))
      ->setSettings([
        'datetime_type' => 'datetime',
      ])
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'short',
        ],
        'weight' => 40,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
